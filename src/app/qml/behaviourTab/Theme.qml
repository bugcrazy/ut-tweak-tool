/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3
import TweakTool 1.0
import QtQuick.Layouts 1.1

import "../components"
import "../components/ListItems" as ListItems
import "../js/shell.js" as Shell

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("System theme")
        flickable: view.flickableItem
    }

    readonly property var themes: [
        "Ambiance",
        "SuruDark",
        "SuruGradient"
    ]

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.Warning {
                iconName: "security-alert"
                text: "<h4>" + i18n.tr("Unity 8 has three available themes") + "</h4>" +
                    i18n.tr("Restart any open apps to use the selected theme.") + " " +
                    i18n.tr("Alternatively, use this to open different apps with different themes.")
            }

            ListItems.SectionDivider { id: temp; text: i18n.tr("Theme selection") }

            ListItems.OptionSelector {
                id: selector
                model: [
                    themes[0] + "            " + i18n.tr("(default theme)"),
                    themes[1] + "             " + i18n.tr("(dark theme)"),
                    themes[2] + "     " + i18n.tr("(test theme, unsupported)")
                ]

                function displayThemeSetting() {
                    var currentTheme = Shell.cmdSystemTheme().trim();
                    for (var i = 0; i < themes.length; i++) {
                        if (currentTheme === themes[i]) {
                            selectedIndex = i;
                            break;
                        }
                    }
                }

                Component.onCompleted: {
                    displayThemeSetting();
                }
                onSelectedIndexChanged:  {
                    Shell.cmdSystemTheme(themes[selectedIndex]);
                }
            }
        }
    }
}
