#!/bin/sh

# Run the command, collecting the output into a variable
RET="$(mount | grep ' on / ' | cut -d\( -f2 | cut -d, -f1)"

# Return `ro` or `rw` respectively
echo "${RET}"
